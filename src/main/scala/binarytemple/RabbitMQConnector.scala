package binarytemple

import akka.actor.{ActorRef, ActorSystem}

import com.thenewmotion.akka.rabbitmq._

case class RabbitMQConnector(exchange: String = "logs")(implicit system: ActorSystem = ActorSystem()) {

  val factory = new ConnectionFactory()
  val connection = system.actorOf(ConnectionActor.props(factory), "rabbitmq")

  def setupPublisher(channel: Channel, self: ActorRef) {
    val queue = channel.queueDeclare().getQueue
    channel.queueBind(queue, exchange, "")
  }

  //Block until the connection is available (or timeout occurs)
  import com.thenewmotion.akka.rabbitmq.ReachConnectionActor
  connection.createChannel(ChannelActor.props(setupPublisher), Some("publisher"))

  def publish(data: Array[Byte], routingKey: String = "") = {
    val publisher = system.actorFor("/user/rabbitmq/publisher")

    def publish(channel: Channel) {
      channel.basicPublish(exchange, routingKey, null, data)
    }
    publisher ! ChannelMessage(publish, dropIfNoChannel = false)
    ()
  }
}
