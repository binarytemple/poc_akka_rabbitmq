import binarytemple.RabbitMQConnector
import java.util.Date

object RabbitMQConnectorMain extends App {
  val rmqc = RabbitMQConnector()
  println("publishing")
  rmqc.publish(s"${new Date().toString}".getBytes)
  rmqc.publish(s"${new Date().toString}".getBytes)
  rmqc.publish(s"${new Date().toString}".getBytes)
  rmqc.publish(s"${new Date().toString}".getBytes)
  rmqc.publish(s"${new Date().toString}".getBytes)
}
