
import sbt._
//import  com.typesafe.sbt.SbtNativePackager.Universal
import Keys._

object ConfBuild extends Build with BuildExtra {

  lazy val sbtIdea = Project("mime_detect", file("."), settings = mainSettings)

  //lazy val mainSettings: Seq[Def.Setting[_]] = Defaults.defaultSettings ++ com.typesafe.sbt.SbtNativePackager.packagerSettings  ++ Seq(
  lazy val mainSettings: Seq[Def.Setting[_]] = Defaults.defaultSettings ++ Seq(
    name := "poc_akka_rabbitmq",
    organization := "binarytemple",
    version := "0.1-SNAPSHOT",
//    sbtVersion in Global := "0.13.0",
    //scalaVersion in Global := "2.10.3",
    scalaVersion := "2.10.3",

//    mappings in Universal <+= (packageBin in Compile) map { jar =>
//      jar -> ("lib/" + jar.getName)
//    },

    resolvers ++= Seq(
      "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
      "Akka Repository" at "http://repo.akka.io/releases/",
      "Spray Repository" at "http://repo.spray.io/",
      "Maven.org Repository" at "http://repo1.maven.org/maven2/",
      "The New Motion Repository" at "http://nexus.thenewmotion.com/content/repositories/releases-public"
    ),

    scalacOptions ++= Seq("-deprecation", "-unchecked", "-feature"),
    libraryDependencies ++= Seq(
       "com.typesafe.akka" %% "akka-actor" % "2.1.2"                  withSources()
      , "com.typesafe.akka" %% "akka-testkit" % "2.1.2"               withSources()
      , "com.typesafe.akka" %% "akka-kernel" % "2.1.2"                withSources()
      , "com.typesafe.akka" %% "akka-transactor" % "2.1.2"            withSources()
      , "com.typesafe.akka" %% "akka-slf4j" % "2.1.2"                 withSources()
      , "com.thenewmotion.akka" % "akka-rabbitmq_2.10" % "1.1.1"      withSources()
      , "org.slf4j" % "slf4j-simple" % "1.5.6"                        withSources()
      , "commons-collections" % "commons-collections" % "3.2.1"       withSources()
      , "commons-configuration" % "commons-configuration" % "1.6"     withSources()
      , "commons-io" % "commons-io" % "2.4"                           withSources()
      , "log4j" % "log4j" % "1.2.16"                                  withSources()
      , "commons-lang" % "commons-lang" % "2.5"                       withSources()
      , "commons-net" % "commons-net" % "3.0.1"                       withSources()
      , "joda-time" % "joda-time" % "1.6.2"                           withSources()
      , "org.mockito" % "mockito-all" % "1.9.0" % "test"              withSources()
      //, "org.specs2" % "specs2_2.10" % "2.3.4"                          withSources()
      , "org.specs2" %% "specs2" % "2.3.4"                            withSources()
      //, "org.specs2" %% "specs2" % "1.13" % "test"                  withSources()
      //, "org.specs2" %% "specs2" % "1.5"                            withSources()
      , "org.mockito" % "mockito-all" % "1.9.0" % "test"              withSources()
      , "com.mortennobel" % "java-image-scaling" % "0.8.5"            withSources()
      //, "net.java.dev.jna" % "jna" % "4.0.0"                       withSources()
      , "net.java.dev.jna" % "jna" % "4.0.0"                       withSources()
      , "com.typesafe.play" %% "play-json" % "2.2.0"                  withSources()
    )
  )
}
