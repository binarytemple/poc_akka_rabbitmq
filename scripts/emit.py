#!/usr/bin/env python
from datetime import datetime
import pika
import sys
import logging

logging.basicConfig()

#simple script to verify that RabbitMQ can be reached.

connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='127.0.0.1'))
channel = connection.channel()

channel.exchange_declare(exchange='logs',
                         type='fanout')

message = ' '.join(sys.argv[1:]) or "info: Hello World %s !" % datetime.now()
channel.basic_publish(exchange='logs',
                      routing_key='',
                      body=message)
print " [x] Sent %r" % (message,)
connection.close()